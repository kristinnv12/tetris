# !/usr/local/bin/python
# -*- coding: utf-8 -*-
import pygame
from game_config import SIZE


class Display:
    __instance = None

    @staticmethod
    def getInstance():
        if Display.__instance is None:
            Display()
        return Display.__instance

    def __init__(self):
        if Display.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            Display.__instance = self

        self.screen = pygame.display.set_mode(SIZE)
