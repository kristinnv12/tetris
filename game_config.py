# !/usr/local/bin/python
# -*- coding: utf-8 -*-
import pygame
import pygame.color as color

SIZE = width, height = 500, 500

GRID_WIDTH = 13
GRID_HEIGHT = 19
BLOCK_SIZE = 20
GRID_COLORS = (35, 40, 43), (19, 20, 22)
BACKGROUND_COLOR = 41, 57, 66
BORDER_COLOR = 142, 147, 138
WHITE_COLOR = 221, 221, 223
BORDER_THICKNESS = 1
EXTERNAL_BORDER_THICKNESS = 2
BLOCK_TOTAL_SIZE = BLOCK_SIZE + BORDER_THICKNESS
GRID_PX_HEIGHT = GRID_HEIGHT * BLOCK_TOTAL_SIZE
GRID_PX_WIDTH = GRID_WIDTH * BLOCK_TOTAL_SIZE
NEXT_TETRA_X_OFFSET = -4
NEXT_TETRA_Y_OFFSET = 6

DB_LOCATION = 'tetris_db'
HIGH_SCORE_TABLE_NAME = 'high_score'
HIGH_SCORE_DISPLAY_COUNT = 10


WIDTH_OFFSET = (SIZE[0]-(GRID_WIDTH*(BLOCK_TOTAL_SIZE)))/2
HEIGHT_OFFSET = SIZE[1]/10
# TODO: use percentages instead of division
MATRIX_AREA = pygame.Rect(
    WIDTH_OFFSET,
    0,
    SIZE[0],
    SIZE[1]
    )

SCORE_AREA = pygame.Rect(
    0,
    0,
    WIDTH_OFFSET,
    SIZE[1]//6
    )

LEVEL_AREA = pygame.Rect(
    0,
    SIZE[1]//6,
    WIDTH_OFFSET,
    SIZE[1]//6
    )


NEXT_TETRA_AREA = pygame.Rect(
    0,
    (SIZE[1]//6)*2,
    WIDTH_OFFSET,
    (SIZE[1]//6)*4
    )


MAIN_MENU_BTNS = [
    'Resume Game',
    'New Game',
    'High Scores',
    'Exit'
    ]

LEVELS = {
    '1': 300,
    '2': 290,
    '3': 280,
    '4': 270,
    '5': 260,
    '6': 250,
    '7': 240,
    '8': 220,
    '9': 210,
    '10': 200,
    '11': 190,
    '12': 180,
    '13': 170,
    '14': 160,
    '15': 150,
    '16': 140,
    '17': 130,
    '18': 120,
    '19': 110,
    '20': 100
}

SCORE_RULE = [0, 40, 100, 300, 1200]

FAST_FRAME_LENGTH = 50
NORMAL_FRAME_LENGHT = LEVELS['1']

BLOCK_PER_LEVEL = 10

O = [
    ['X', 'X'],
    ['X', 'X']
    ]

S = [
    ['O', 'X', 'X'],
    ['X', 'X', 'O'],
    ['O', 'O', 'O']
    ]

Z = [
    ['X', 'X', 'O'],
    ['O', 'X', 'X'],
    ['O', 'O', 'O']
    ]

T = [
    ['O', 'X', 'O'],
    ['X', 'X', 'X'],
    ['O', 'O', 'O']
    ]

J = [
    ['X', 'O', 'O'],
    ['X', 'X', 'X'],
    ['O', 'O', 'O']
    ]

L = [
    ['O', 'O', 'X'],
    ['X', 'X', 'X'],
    ['O', 'O', 'O']
    ]

I = [
    ['O', 'X', 'O', 'O'],
    ['O', 'X', 'O', 'O'],
    ['O', 'X', 'O', 'O'],
    ['O', 'X', 'O', 'O']
    ]


O_color = 255, 215, 122
S_color = 127, 182, 133
Z_color = 222, 108, 131
T_color = 143, 88, 173
J_color = 68, 122, 155
L_color = 255, 155, 66
I_color = 163, 217, 255

O_letter = 'O'
S_letter = 'S'
Z_letter = 'Z'
T_letter = 'T'
J_letter = 'J'
L_letter = 'L'
I_letter = 'I'

SHAPES = [
        (O, O_color, O_letter),
        (S, S_color, S_letter),
        (Z, Z_color, Z_letter),
        (T, T_color, T_letter),
        (J, J_color, J_letter),
        (L, L_color, L_letter),
        (I, I_color, I_letter)
        ]
