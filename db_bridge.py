# !/usr/local/bin/python
# -*- coding: utf-8 -*-
import sqlite3
from game_config import DB_LOCATION, HIGH_SCORE_TABLE_NAME, \
    HIGH_SCORE_DISPLAY_COUNT


class Db_bridge:
    __instance = None

    @staticmethod
    def getInstance():
        if Db_bridge.__instance is None:
            Db_bridge()
        return Db_bridge.__instance

    def __init__(self):
        if Db_bridge.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            Db_bridge.__instance = self

        self.connection = sqlite3.connect(DB_LOCATION)
        self.cursor = self.connection.cursor()
        self.cursor.execute('CREATE TABLE IF NOT EXISTS {tn} ({f})'.format(
            tn=HIGH_SCORE_TABLE_NAME,
            f='id integer PRIMARY KEY, score integer NOT NULL, name text NOT NULL'
            ))
        self.connection.commit()

    def record_high_score(self, name, score):
        self.cursor.execute('INSERT INTO {tn} ({f}) VALUES(\'{s}\', \'{n}\');'.format(
            tn=HIGH_SCORE_TABLE_NAME,
            f='score, name',
            s=score,
            n=name
            ))
        self.connection.commit()

    def get_high_scores(self):
        rows = self.cursor.execute('SELECT * FROM {tn} ORDER BY score DESC LIMIT {hsdc}'.format(
            tn=HIGH_SCORE_TABLE_NAME,
            hsdc=HIGH_SCORE_DISPLAY_COUNT
            ))
        return rows

    # TODO: save game state that is state, matrix, score, level, user...
