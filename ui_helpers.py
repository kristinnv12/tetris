# !/usr/local/bin/python
# -*- coding: utf-8 -*-
import sys
import pygame
from game_config import SIZE, WHITE_COLOR, BACKGROUND_COLOR,\
    HIGH_SCORE_DISPLAY_COUNT, MAIN_MENU_BTNS
from db_bridge import Db_bridge
from display import Display

screen = Display.getInstance().screen


def resume_game():
    return 0


def new_game():
    return 1


def exit_game():
    sys.exit()


def high_scores():
    db = Db_bridge.getInstance()
    scores = db.get_high_scores()
    font = pygame.font.Font('freesansbold.ttf', 26)
    screen.fill(BACKGROUND_COLOR)
    for i, score in enumerate(scores):
        line = font.render((score[2] + ' : ' + str(score[1])), True, WHITE_COLOR, None)
        lineRect = line.get_rect()
        lineRect.center = (SIZE[1]//2, (SIZE[0]//(HIGH_SCORE_DISPLAY_COUNT+1))*(i+1))
        screen.blit(line, lineRect)

    pygame.display.flip()

    while 1:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_h:
                        return 2


class ButtonBox:

    def __init__(self, x, y, w, h, text):
        self.rect = pygame.Rect(x, y, w, h)
        self.color = WHITE_COLOR
        self.text = text
        self.font = pygame.font.Font('freesansbold.ttf', 26)
        self.txt_surface = self.font.render(text, True, self.color)

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            # TODO: ENUM ?
            if self.rect.collidepoint(event.pos):
                if self.text == MAIN_MENU_BTNS[0]:
                    return resume_game()
                if self.text == MAIN_MENU_BTNS[1]:
                    return new_game()
                if self.text == MAIN_MENU_BTNS[2]:
                    return high_scores()
                if self.text == MAIN_MENU_BTNS[3]:
                    return exit_game()
        return -1

    def draw(self):
        screen.blit(self.txt_surface, (self.rect.x+7, self.rect.y+7))
        pygame.draw.rect(screen, self.color, self.rect, 2)


def get_user_name():
    font = pygame.font.Font('freesansbold.ttf', 26)

    greeting = font.render('What is your name?', True, WHITE_COLOR, None)
    greetingRect = greeting.get_rect()
    greetingRect.center = (SIZE[0]//2, (SIZE[1]//10)*2)

    clock = pygame.time.Clock()
    input_box = pygame.Rect(100, (SIZE[1]//10)*3, SIZE[0]//2, 40)
    input_box.centerx = SIZE[0]//2
    user = ''
    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    return user
                elif event.key == pygame.K_BACKSPACE:
                    user = user[:-1]
                else:
                    if len(user) < 10:
                        user += event.unicode

            screen.fill(BACKGROUND_COLOR)
            screen.blit(greeting, greetingRect)
            txt_surface = font.render(user, True, WHITE_COLOR)
            screen.blit(txt_surface, (input_box.x+8, input_box.y+8))
            pygame.draw.rect(screen, WHITE_COLOR, input_box, 2)

            pygame.display.flip()
            clock.tick(30)


def game_over():
    font = pygame.font.Font('freesansbold.ttf', 32)
    text = font.render('GAME OVER !!!', True, WHITE_COLOR, None)
    textRect = text.get_rect()
    textRect.center = (SIZE[0]//2, SIZE[1]//2)
    screen.blit(text, textRect)
    pygame.display.flip()

    pygame.time.delay(4000)


def draw_main_menu_buttons(buttons):
    screen.fill(BACKGROUND_COLOR)
    for btn in buttons:
        btn.draw()
    pygame.display.flip()


def main_menu(state):
    buttons = []
    for i, main_menu_button in enumerate(MAIN_MENU_BTNS):
        if main_menu_button == 'Resume Game' and state == 'over':
            pass
        else:
            button = ButtonBox(
                SIZE[1]//4,
                (SIZE[0]//7)*i+(SIZE[0]//10),
                SIZE[1]//2,
                40,
                main_menu_button,
                )

            buttons.append(button)

    draw_main_menu_buttons(buttons)
    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return 0
            for btn in buttons:
                return_value = btn.handle_event(event)
                if return_value == 2:
                    draw_main_menu_buttons(buttons)
                elif return_value == -1:
                    pass
                else:
                    return return_value
