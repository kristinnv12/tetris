# !/usr/local/bin/python
# -*- coding: utf-8 -*-
from game_config import *
from display import Display
import pygame

screen = Display.getInstance().screen


class Tetra:
    def __init__(self, shape, color, letter, x, y):
        self.shape = shape
        self.color = color
        self.x = x
        self.y = y
        self.letter = letter

    def get_rects(self):

        tetra_rects = []
        for y, row in enumerate(self.shape):
            for x, exists in enumerate(row):
                if exists == 'X':
                    tetra_rect = pygame.Rect(
                        self.get_x()+WIDTH_OFFSET+(x*(BLOCK_TOTAL_SIZE)),
                        self.get_y()+HEIGHT_OFFSET+(y*(BLOCK_TOTAL_SIZE)),
                        BLOCK_SIZE,
                        BLOCK_SIZE)

                    tetra_rects.append(tetra_rect)

        return tetra_rects

    def get_x(self):
        return self.x*(BLOCK_TOTAL_SIZE)

    def get_y(self):
        return self.y*(BLOCK_TOTAL_SIZE)


class Game:
    matrix = []

    def __init__(self, state):
        self.state = state
        self.__score = None
        self.__level = None
        self.matrix = []
        for x in range(GRID_HEIGHT):
            self.matrix.append([])
            for y in range(GRID_WIDTH):
                self.matrix[x].append('o')

    @property
    def score(self):
        return self.__score

    @score.setter
    def score(self, value):
        if value != self.__score:
            self.__score = value
            self.draw_score()

    @property
    def level(self):
        return self.__level

    @level.setter
    def level(self, value):
        if value != self.__level:
            self.__level = value
            self.draw_level()

    def add_tetra_to_matrix(self, tetra: Tetra):
        for y, row in enumerate(tetra.shape):
            for x, exists in enumerate(row):
                if exists == 'X':
                    self.matrix[int(y+tetra.y)][int(x+tetra.x)] = tetra.letter

    def draw_score(self):
        # TODO: DRY
        screen.fill(BACKGROUND_COLOR, SCORE_AREA)
        font = pygame.font.Font('freesansbold.ttf', 16)
        score = font.render('Score: ' + str(self.score), True, WHITE_COLOR, None)
        scoreRect = score.get_rect()
        scoreRect.center = (SIZE[0]//10, SIZE[1]//8)
        screen.blit(score, scoreRect)

    def draw_level(self):
        # TODO: DRY
        screen.fill(BACKGROUND_COLOR, LEVEL_AREA)
        font = pygame.font.Font('freesansbold.ttf', 16)
        level = font.render('Level: ' + str(self.level), True, WHITE_COLOR, None)
        levelRect = level.get_rect()
        levelRect.center = (SIZE[0]//10, SIZE[1]//4)
        screen.blit(level, levelRect)

    def draw_grid(self):
        screen.fill(BACKGROUND_COLOR, MATRIX_AREA)
        for y in range(len(self.matrix)):
            for x in range(len(self.matrix[y])):
                grid_rect = pygame.Rect(
                    WIDTH_OFFSET+(x*(BLOCK_TOTAL_SIZE)),
                    HEIGHT_OFFSET+(y*(BLOCK_TOTAL_SIZE)),
                    BLOCK_SIZE,
                    BLOCK_SIZE
                    )

                block_letter = self.matrix[y][x]

                curr_color = GRID_COLORS[x % 2]

                for shape in SHAPES:
                    if block_letter in shape:
                        curr_color = shape[1]

                pygame.draw.rect(screen, curr_color, grid_rect)

        top_line = pygame.Rect(
            WIDTH_OFFSET,
            HEIGHT_OFFSET,
            GRID_PX_WIDTH,
            EXTERNAL_BORDER_THICKNESS
            )

        bottom_line = pygame.Rect(
            WIDTH_OFFSET,
            HEIGHT_OFFSET+GRID_PX_HEIGHT,
            GRID_PX_WIDTH,
            EXTERNAL_BORDER_THICKNESS
            )

        left_line = pygame.Rect(
            WIDTH_OFFSET,
            HEIGHT_OFFSET,
            EXTERNAL_BORDER_THICKNESS,
            GRID_PX_HEIGHT
            )

        right_line = pygame.Rect(
            WIDTH_OFFSET+GRID_PX_WIDTH,
            HEIGHT_OFFSET,
            EXTERNAL_BORDER_THICKNESS,
            GRID_PX_HEIGHT+EXTERNAL_BORDER_THICKNESS
            )

        pygame.draw.rect(screen, BORDER_COLOR, top_line)
        pygame.draw.rect(screen, BORDER_COLOR, bottom_line)
        pygame.draw.rect(screen, BORDER_COLOR, left_line)
        pygame.draw.rect(screen, BORDER_COLOR, right_line)

    def drop_full_rows(self):
        full_rows = []
        for y in range(len(self.matrix)):
            count = 0
            for x in range(len(self.matrix[y])):
                if self.matrix[y][x] != 'o':
                    count += 1
                if count >= GRID_WIDTH:
                    full_rows.append(y)
        for row in full_rows:
            del self.matrix[row]
            self.matrix.insert(0, [])
            for i in range(GRID_WIDTH):
                self.matrix[0].append('o')
        self.score += SCORE_RULE[len(full_rows)] * self.level

    def pretty_print_matrix(self):
        for x in range(len(self.matrix)):
            print(self.matrix[x])
