# !/usr/local/bin/python
# -*- coding: utf-8 -*-

import sys
import time
from copy import deepcopy
import pygame
import numpy as np
from random import randrange
from game_config import *
from tetris_objects import Game, Tetra
from db_bridge import Db_bridge
from ui_helpers import *
from display import Display


def get_new_tetra(next_tetra):
    if next_tetra:
        tetra_x_offset = NEXT_TETRA_X_OFFSET
        tetra_y_offset = NEXT_TETRA_Y_OFFSET
    else:
        tetra_x_offset = (GRID_WIDTH // 2) - 1
        tetra_y_offset = 0

    shape = SHAPES[randrange(len(SHAPES))]
    shape_width = len(shape[0][0])
    tetra = Tetra(
        shape[0],
        shape[1],
        shape[2],
        tetra_x_offset,
        tetra_y_offset
        )
    return tetra


def out_of_bounds(tetra: Tetra, direction):
    for y, row in enumerate(tetra.shape):
        for x, exists in enumerate(row):
            if exists == 'X':
                if direction == 'NONE':
                    if x+tetra.x < 0 or \
                       x+tetra.x >= GRID_WIDTH or \
                       y+tetra.y >= GRID_HEIGHT:
                        return True
                if direction == 'LEFT':
                    if x+tetra.x <= 0:
                        return True
                if direction == 'RIGHT':
                    if x+tetra.x >= GRID_WIDTH - 1:
                        return True
                if direction == 'BOTTOM':
                    if y+tetra.y >= GRID_HEIGHT - 1:
                        return True
    return False


def tetra_obstructed(tetra: Tetra, matrix, direction):
    for y, row in enumerate(tetra.shape):
        for x, exists in enumerate(row):
            if exists == 'X':
                    y_offset = int(tetra.y)+y
                    x_offset = int(tetra.x)+x

                    if direction == 'NONE':
                        pass
                    elif direction == 'BOTTOM':
                        y_offset += 1
                    elif direction == 'LEFT':
                        x_offset -= 1
                    elif direction == 'RIGHT':
                        x_offset += 1
                    try:
                        if matrix[y_offset][x_offset] != 'o':
                            return True
                    except IndexError as e:
                        return True
    return False


def tetra_dead(tetra: Tetra, matrix):
    return out_of_bounds(tetra, 'BOTTOM') or tetra_obstructed(tetra, matrix, 'BOTTOM')


def rotate_tetra(tetra: Tetra):
    np_shape = np.array(tetra.shape)
    rotated_np_shape = np.rot90(np_shape)
    return list(rotated_np_shape)


def refresh_screen(tetra: Tetra, game: Game, next_tetra: Tetra=''):
    game.draw_grid()
    draw_tetra(tetra)
    if next_tetra:
        screen.fill(BACKGROUND_COLOR, NEXT_TETRA_AREA)
        draw_tetra(next_tetra)
    pygame.display.flip()


def drop_tetra(tetra: Tetra, matrix):
    tetra_not_dead = not tetra_dead(tetra, matrix)
    while tetra_not_dead:
        tetra.y += 1
        tetra_not_dead = not tetra_dead(tetra, matrix)


def draw_tetra(tetra: Tetra):
    for tetra_rect in tetra.get_rects():
        pygame.draw.rect(screen, tetra.color, tetra_rect)


def main():
    global NORMAL_FRAME_LENGHT
    global screen

    pygame.init()

    screen = Display.getInstance().screen
    db = Db_bridge.getInstance()
    pygame.display.set_caption('Tetris')

    number_of_tetra = 0
    current_frame_length = NORMAL_FRAME_LENGHT
    time_tick = 0
    dead_on_fast_mode = False
    draw_stats = 0

    game = Game('active')
    next_tetra = get_new_tetra(True)
    current_tetra = get_new_tetra(False)
    user = get_user_name()
    screen.fill(BACKGROUND_COLOR)
    refresh_screen(current_tetra, game, next_tetra)
    game.score = 0
    game.level = 1

    while 1:
        # TODO: Be able to switch trade blocks (next tetrac/current tetra)
        if time_tick > current_frame_length:
            time_tick = 0
        time_tick += 1

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    main_menu_action = main_menu(game.state)
                    if main_menu_action:
                        return 1
                    draw_stats = 1

                if event.key == pygame.K_SPACE:
                    drop_tetra(current_tetra, game.matrix)
                    time_tick = current_frame_length - 1
                if event.key == pygame.K_LEFT:
                    if not out_of_bounds(current_tetra, 'LEFT'):
                        if not tetra_obstructed(current_tetra, game.matrix, 'LEFT'):
                            current_tetra.x -= 1
                            refresh_screen(current_tetra, game)

                if event.key == pygame.K_RIGHT:
                    if not out_of_bounds(current_tetra, 'RIGHT'):
                        if not tetra_obstructed(current_tetra, game.matrix, 'RIGHT'):
                            current_tetra.x += 1
                            game.draw_grid()
                            refresh_screen(current_tetra, game)

                if event.key == pygame.K_DOWN and not dead_on_fast_mode:
                    current_frame_length = FAST_FRAME_LENGTH
                    time_tick = current_frame_length
                if event.key == pygame.K_UP:
                    backup = deepcopy(current_tetra)
                    backup.shape = rotate_tetra(backup)
                    if not out_of_bounds(backup, 'NONE'):
                        if not tetra_obstructed(backup, game.matrix, 'NONE'):
                            current_tetra = backup
                            refresh_screen(current_tetra, game)

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_DOWN:
                    dead_on_fast_mode = False
                    current_frame_length = NORMAL_FRAME_LENGHT

        if time_tick == current_frame_length:
            if number_of_tetra < len(LEVELS) * BLOCK_PER_LEVEL:
                current_level = (number_of_tetra // BLOCK_PER_LEVEL) + 1
                game.level = current_level
                NORMAL_FRAME_LENGHT = LEVELS[str(current_level)]

            if game.state == 'dead':
                current_tetra = deepcopy(next_tetra)
                current_tetra.x = (GRID_WIDTH // 2) - 1
                current_tetra.y = 0
                next_tetra = get_new_tetra(True)
                refresh_screen(current_tetra, game, next_tetra)
                if tetra_dead(current_tetra, game.matrix):
                    game.state = 'over'
                    game.add_tetra_to_matrix(current_tetra)
                    game.draw_grid()
                    db.record_high_score(user, game.score)
                    game_over()
                    main_menu_action = main_menu(game.state)
                    if main_menu_action:
                        return 1

                game.state = 'active'

            if tetra_dead(current_tetra, game.matrix):
                game.state = 'dead'
                number_of_tetra += 1
                if FAST_FRAME_LENGTH == current_frame_length:
                    # TODO: Game could be more forgiving when dead on fast, that is it should stop fastmode before it dies
                    dead_on_fast_mode = True
                    current_frame_length = NORMAL_FRAME_LENGHT
                game.add_tetra_to_matrix(current_tetra)
                refresh_screen(current_tetra, game, next_tetra)
                game.drop_full_rows()
                time_tick = current_frame_length
            else:
                refresh_screen(current_tetra, game, next_tetra)
                current_tetra.y += 1
            if draw_stats:
                game.draw_score()
                game.draw_level()
                draw_stats = 0

        pygame.display.flip()

if __name__ == "__main__":
    play_game = True
    while play_game:
        play_game = main()
